<div class="uc_home">
<div class="banner">
<h1 class="title">Welcome to Union Church of Manila</h1>
Lorem ipsum dolor sit amet, consectetur adipiscing elit. In ac erat mi. Integer mattis vel augue a venenatis. Phasellus vel dignissim risus, ut volutpat enim. Fusce euismod rutrum fringilla.

</div>
<div class="row-fluid border-top">
<div class="span3">
<a class="img_cont" href="get-to-know-us">
<div>
<img alt="" src="http://localhost/ts-union-church-of-manila/wp-content/uploads/2013/11/home_img1.jpg" />
<div class="title">Get to Know Us</div>
<div class="cover gtku">&nbsp;</div>
</div>
</a>
<ul class="list">
	<li>Integer nec lorem metus</li>
	<li>Maecenas sit amet felis</li>
	<li>Congue, elementum purus</li>
	<li>Porttitor dui</li>
</ul>
</div>
<div class="span3">
<a class="img_cont" href="events">
<div>
<img alt="" src="http://localhost/ts-union-church-of-manila/wp-content/uploads/2013/11/home_img2.jpg" />
<div class="title">Events</div>
<div class="cover events">&nbsp;</div>
</div>
</a>
<ul class="list">
	<li>Integer nec lorem metus</li>
	<li>Maecenas sit amet felis</li>
	<li>Congue, elementum purus</li>
	<li>Porttitor dui</li>
</ul>
</div>
<div class="span3">
<a class="img_cont" href="media">
<div>
<img alt="" src="http://localhost/ts-union-church-of-manila/wp-content/uploads/2013/11/home_img3.jpg" />
<div class="title">Media</div>
<div class="cover media_">&nbsp;</div>
</div>
</a>
<ul class="list">
	<li>Integer nec lorem metus</li>
	<li>Maecenas sit amet felis</li>
	<li>Congue, elementum purus</li>
	<li>Porttitor dui</li>
</ul>
</div>
<div class="span3 ">
<a class="img_cont" href="ministries">
<div>
<img alt="" src="http://localhost/ts-union-church-of-manila/wp-content/uploads/2013/11/home_img4.jpg" />
<div class="title">Ministries</div>
<div class="cover ministries">&nbsp;</div>
</div>
</a>
<ul class="list">
	<li>Integer nec lorem metus</li>
	<li>Maecenas sit amet felis</li>
	<li>Congue, elementum purus</li>
	<li>Porttitor dui</li>
</ul>
</div>
</div>
<div class="row-fluid border_group">
<div class="span3">
<div class="border_1"> </div>
</div>
<div class="span3">
<div class="border_2"> </div>
</div>
<div class="span3">
<div class="border_3"> </div>
</div>
<div class="span3">
<div class="border_4"> </div>
</div>
</div>
</div>