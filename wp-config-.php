<?php
/**
 * The base configurations of the WordPress.
 *
 * This file has the following configurations: MySQL settings, Table Prefix,
 * Secret Keys, WordPress Language, and ABSPATH. You can find more information
 * by visiting {@link http://codex.wordpress.org/Editing_wp-config.php Editing
 * wp-config.php} Codex page. You can get the MySQL settings from your web host.
 *
 * This file is used by the wp-config.php creation script during the
 * installation. You don't have to use the web site, you can just copy this file
 * to "wp-config.php" and fill in the values.
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'ismb_blog');

/** MySQL database username */
define('DB_USER', 'root');

/** MySQL database password */
define('DB_PASSWORD', '');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         '( q`4k!y4WR~A(AB9v0`H;[<]j#,~Hc(lemoL41Bj<v3yHbgL>Y|/c)#[l>=G%tW');
define('SECURE_AUTH_KEY',  ']O^P^yEaYIF}P-h)-A_y_4V,wc#Y*ju{?t_(Sl(LW1=K6T2OhrARUO.dK.kZ<9Kr');
define('LOGGED_IN_KEY',    'QI[KUn./PC#$:RT-%r&aJXYf5@u9~uWs;p!e+^T7MpL}L)sCuVIhMX};nxL-8@uw');
define('NONCE_KEY',        'mM(L6P;fM+UY7-.S`54Mbl2ELj #~Zb5/M)O5@f1sk;Vsr;N!|CfKE[Qc>m1M9b ');
define('AUTH_SALT',        '%,gdUnf=S/Vw[u.4U8;L~T|`ffcN@mr*R$yn_&D#9=*V:*qNLEVS?3Cjo0%2G-Ux');
define('SECURE_AUTH_SALT', 'vqv}#%  -F4NvApAzL;5CCyWS|sg={(J<=JRVSJv[B)JUN)%FNh]c=e}<FQIbd6@');
define('LOGGED_IN_SALT',   'si1&F7m N=1)ho/64ot]iPAtT-vD1hIlMNr8yj9]:F{Z$t2Zq%ysQ!4c*6F7SWSy');
define('NONCE_SALT',       '.V;Z$O5W{e|tw?I=8]:/%egvtw-ly8M~ozzsgz??rOu7M%}e`!>I;jotJhL;6>ii');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each a unique
 * prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'ismb_';

/**
 * WordPress Localized Language, defaults to English.
 *
 * Change this to localize WordPress. A corresponding MO file for the chosen
 * language must be installed to wp-content/languages. For example, install
 * de_DE.mo to wp-content/languages and set WPLANG to 'de_DE' to enable German
 * language support.
 */
define('WPLANG', '');

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');