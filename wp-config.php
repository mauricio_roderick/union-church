<?php
/**
 * The base configurations of the WordPress.
 *
 * This file has the following configurations: MySQL settings, Table Prefix,
 * Secret Keys, WordPress Language, and ABSPATH. You can find more information
 * by visiting {@link http://codex.wordpress.org/Editing_wp-config.php Editing
 * wp-config.php} Codex page. You can get the MySQL settings from your web host.
 *
 * This file is used by the wp-config.php creation script during the
 * installation. You don't have to use the web site, you can just copy this file
 * to "wp-config.php" and fill in the values.
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'union-church');
// define('DB_NAME', 'unionchu_tsumodb');

/** MySQL database username */
define('DB_USER', 'root');
// define('DB_USER', 'unionchu_tsumo');

/** MySQL database password */
define('DB_PASSWORD', '');
// define('DB_PASSWORD', '7612hjv2g!!f');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'kRBM}H-8^GL2XV-Od_+]33vm7y<s~.r2lU5<pNpAQU|]K:Eik+Ajb-y]=4 /Q3DV');
define('SECURE_AUTH_KEY',  'CTO!.r.#MvuR`]CHSX|| !i^hElu7Oo/&FA$W90F ?lZu3CP|[0;w(1W.;@+R-Q ');
define('LOGGED_IN_KEY',    'tRdHR8cd2qoykp=([K:RqptvS#r<QMuq#W(}]zF+3e+>^+91ho{]%]USJ~KyO50L');
define('NONCE_KEY',        'sU.cWG]xk(J+|c#R9B+J1zJfQ6O)1/6c0n&tIuj50MSQ1a7,A128plcn@NNSmQ@]');
define('AUTH_SALT',        '99`jm!8~ds*XkS+`~`GY$5*u]~3S:([d6O+&?N:T_3q0j]`YNjs0+EZ!Y4c0StU!');
define('SECURE_AUTH_SALT', 'v3.2~6#>m1m6Z3+i6(^l,N+-xf$Ua7`<`E#*h<ku;HyA[|>!S8y?k({/I%=tgf[f');
define('LOGGED_IN_SALT',   '#ar+)+FY,xq(Qo)TktB$/O1+h1v}FV{kZDZ{H:wZbW-jsp>qry|G*|Y~mxfrsY-O');
define('NONCE_SALT',       'j DUD#3llguKcS)V)_gQuH-7`qcw&P&_q(mH|j/k.6!:rA`oYFF x{I(8{Icue&u');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each a unique
 * prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'uc_';

/**
 * WordPress Localized Language, defaults to English.
 *
 * Change this to localize WordPress. A corresponding MO file for the chosen
 * language must be installed to wp-content/languages. For example, install
 * de_DE.mo to wp-content/languages and set WPLANG to 'de_DE' to enable German
 * language support.
 */
define('WPLANG', '');

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
