<div class="item">

<a href="<?php the_permalink(); ?>" title="<?php printf( __('Read %s', 'blankslate'), the_title_attribute('echo=0') ); ?>" rel="bookmark" class="entry-title" ><?php the_title(); ?></a>

<?php
	echo wp_trim_words( strip_tags($post->post_content), 30, '...' );
?>
</div> 