<div class="footer">
	<div class="container">
	
		<div class="col_item small">
			<div class="group">
				<a href="<?php echo site_url('worship-services') ?>" class="heading">WORSHIP SERVICES</a>
				<div class="spacing"></div>
                
                <a href="<?php echo site_url('history') ?>" class="heading">HISTORY</a>
				<div class="spacing"></div>
                
                <a href="<?php echo site_url('belief-statement') ?>" class="heading">BELIEF STATEMENT</a>
				<div class="spacing"></div>   
                          
				<a href="<?php echo site_url('vision-mission') ?>" class="heading">VISION & MISSION</a>
				<div class="spacing"></div>
                
                <div class="heading">OUR STAFF</div>
                <ul class="list">
					<li><a href="<?php echo site_url('pastors') ?>">Pastors</a></li>
					<li><a href="<?php echo site_url('ministry-staff') ?>">Ministry Staff</a></li>
					<li><a href="<?php echo site_url('support-staff') ?>">Support Staff</a></li>
                </ul>
				<div class="spacing"></div>
                
                <a href="<?php echo site_url('council-of-elders') ?>" class="heading">COUNCIL OF ELDERS</a>
				<div class="spacing"></div>
                
                <div class="heading">EVENTS</div>
	            <ul class="list">
					<li><a href="<?php echo site_url('centennial') ?>">Special Events - Centennial</a></li>
					<li><a href="<?php echo site_url('calendar') ?>">Church Calendar</a></li>
                </ul>
			</div>
		</div>
		
		<div class="col_item small">
			<div class="group">
				<div class="heading">MEDIA</div>
	            <ul class="list">
					<li><a href="<?php echo site_url('media') ?>">Sermons</a></li>
                </ul>
				<div class="spacing"></div>
				
				<div class="heading">AGE & STAGE</div>
				<ul class="list">
					<li><a href="<?php echo site_url('children') ?>">Children</a></li>
					<li><a href="<?php echo site_url('youth') ?>">Youth</a></li>
					<li><a href="<?php echo site_url('college-young-adults') ?>">College & Youth Adults</a></li>
				</ul>
				<div class="spacing"></div>
				
				<div class="heading">CARE MINISTRIES</div>
				<ul class="list">
					<li><a href="<?php echo site_url('congregational-care') ?>">Congregational Care</a></li>
					<li>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<a href="<?php echo site_url('counseling') ?>">Counseling</a></li>
					<li>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<a href="<?php echo site_url('funerals') ?>">Funerals</a></li>
                    <li>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<a href="<?php echo site_url('prayer') ?>">Prayer</a></li>
					<li>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<a href="<?php echo site_url('visitations') ?>">Visitations</a></li>
					<li>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<a href="<?php echo site_url('weddings') ?>">Weddings</a></li>
                    <li><a href="<?php echo site_url('medical-ministries') ?>">Medical Ministries</a></li>
					<li><a href="<?php echo site_url('support-groups') ?>">Support Groups</a></li>
					<li><a href="<?php echo site_url('congregational-hospitality') ?>">Congregational Hospitality</a></li>
				</ul>
			</div>
		</div>
		
		<div class="col_item med">
			<div class="group">		
				<div class="heading">DISCIPLESHIP</div>
				<ul class="list">
					<li><a href="<?php echo site_url('adult-christian-education') ?>">Adult Christian Education</a></li>
					<li><a href="<?php echo site_url('small-discipleship-groups') ?>">Small Discipleship Groups</a></li>
				</ul>
                <div class="dbl_spacing"></div>
				
				<div class="heading">FELLOWSHIP & MEMBERSHIP</div>
				<ul class="list">
					<li><a href="<?php echo site_url('affinity-groups') ?>">Affinity Groups</a></li>
					<li><a href="<?php echo site_url('membership') ?>">Membership</a></li>
				</ul>
				<div class="dbl_spacing"></div>
                
				<div class="heading">MISSIONS & BENEVOLENCE</div>
				<ul class="list">
					<li><a href="<?php echo site_url('missions') ?>">Missions</a></li>
					<li><a href="<?php echo site_url('benevolence') ?>">Benevolence</a></li>
				</ul>
				<div class="dbl_spacing"></div>
                
                <div class="heading">OUTREACH & EVANGELISM</div>
				<ul class="list">
					<li><a href="<?php echo site_url('evangelism') ?>">Evangelism</a></li>
					<li><a href="<?php echo site_url('international-ministries') ?>">International Ministries</a></li>
                    <li><a href="<?php echo site_url('library') ?>">Library</a></li>
				</ul>
			</div>		
		</div>
		
		<div class="col_item med">
			<div class="group">
				<a href="<?php echo site_url('worship') ?>" class="heading">WORSHIP</a>
				<div class="dbl_spacing"></div>
                
				<a href="<?php echo site_url('give') ?>" class="heading">GIVE</a>
				<div class="dbl_spacing"></div>
                
				<a href="<?php echo site_url('volunteer-opportunities') ?>" class="heading">VOLUNTEER OPPORTUNITIES</a>
				<div class="dbl_spacing"></div>
                
                <a href="<?php echo site_url('employment-opportunities') ?>" class="heading">EMPLOYMENT OPPORTUNITIES</a>
				<div class="dbl_spacing"></div>
                
				<a href="<?php echo site_url('contact-us') ?>" class="heading">CONTACT US</a>
				<div class="dbl_spacing"></div>
                
				<a href="<?php echo site_url('location-directions') ?>" class="heading">LOCATION & DIRECTIONS</a>
			</div>
		</div>
		
		<div class="last">

			<div>©2014 Union Church of Manila</div>
			<a class="padded_link" href="<?php echo site_url() ?>" >Private Policy</a> | <a class="padded_link" href="<?php echo site_url() ?>" >Terms and Conditions</a>
		</div>
		<div class="clear_both"></div>
	</div>
	
</div>
<?php wp_footer(); ?>
</body>
</html>