<?php defined('ABSPATH') or die("No direct script access allowed");

require('download_helper.php');

function add_query_vars_filter( $vars ){
	$request_uri = explode('/', $_SERVER["REQUEST_URI"]);
	if(in_array('media-archive', $request_uri))
	{
		$vars[] = "m_month";
		$vars[] = "m_year";
	}
	return $vars;
}
add_filter( 'query_vars', 'add_query_vars_filter' );

function search_join_postmeta($join) {
    global $wp_query, $wpdb;
    if (!empty($wp_query->query_vars['s']))
    $join .= "LEFT JOIN $wpdb->postmeta ON $wpdb->posts.ID = $wpdb->postmeta.post_id ";
    
	return $join;
}
add_filter('posts_join', 'search_join_postmeta');

function search_where( $where ) {
	global $wp_query, $wpdb;
	if(! empty($wp_query->query_vars['s']) )
	{
		$search_terms = explode(' ', $wp_query->query_vars['s']);
		
		$new_where_query = $search_term_query = array();
		foreach($search_terms as $search_term)
		{
			$search_term_query[] = "((uc_posts.post_title LIKE '%{$search_term}%') OR (uc_posts.post_content LIKE '%{$search_term}%') OR (uc_postmeta.meta_value LIKE '%{$search_term}%'))";
		}
		
		$search_term_query = '('.implode(' AND ', $search_term_query).')';
		$new_where_query[] = $search_term_query;
		$new_where_query[] = "(uc_posts.post_password = '')";
		$new_where_query[] = "(uc_posts.post_status = 'publish')";
		
		$public_post_types = get_post_types(array('public' => true));
		if(count($public_post_types) > 0)
		{
			$where_post_types = "uc_posts.post_type IN ('".implode("','", $public_post_types)."')";
			$new_where_query[] = $where_post_types;
		}
		
		$new_where_query = ' AND '.implode(' AND ', $new_where_query);
		$where = $new_where_query;
	}
	return $where;
}
add_filter( 'posts_where' , 'search_where' );

function search_groupby($groupby) {
    global $wpdb;
    $groupby = "{$wpdb->posts}.ID";
    return $groupby;
}
add_filter( 'posts_groupby', 'search_groupby' );

function ucm_force_download()
{
	if(isset($_GET['download']))
	{
		$audio = get_field('ucm_sermon_file');
		
		if($audio)
		{
			$filename = explode('/', $audio['url']);
			$data = file_get_contents($audio['url']); 
			force_download(end($filename), $data);
		}
	}
}

function get_give_link($pages = '')
{
	$give = get_post( 299 );
	return ($give && $give->post_status == 'publish') ? '<a href="'.site_url('give').'">Give</a>' : '';
}


function ucm_pagination($pages = '')
{
	return ($pages) ? '<div class="pagination" >'.$pages.'</div>' : '';
}

add_action( 'wp_enqueue_scripts', 'wpbootstrap_scripts_with_jquery' );
function wpbootstrap_scripts_with_jquery()
{
	// Register the script like this for a theme:
	wp_register_script( 'custom-script', get_template_directory_uri() . '/bootstrap/js/bootstrap.min.js', array( 'jquery' ) );
	// For either a plugin or a theme, you can then enqueue the script:
	wp_enqueue_script( 'custom-script' );
}

add_action( 'init', 'create_post_type' );
function create_post_type() {
	$ucm_staff_labels = array(
				'name'               => 'Staffs',
				'singular_name'      => 'Staff',
				'add_new'            => 'Add Staff',
				'add_new_item'       => 'Add New Staff',
				'edit_item'          => 'Edit Staff',
				'new_item'           => 'New Staff',
				'all_items'          => 'All Staffs',
				'view_item'          => 'View Staff',
				'search_items'       => 'Search Staffs',
				'not_found'          => 'No staffs found',
				'not_found_in_trash' => 'No staffs found in Trash',
				'parent_item_colon'  => '',
				'menu_name'          => 'Staffs'
			);
			
	
	$args = array(
				'labels' => $ucm_staff_labels,	
				'public' => true,
				'publicly_queryable' => true,
				'show_ui' => true,
				'show_in_menu ' => true,
				'menu_position' => 2,
				'map_meta_cap' => true,
				'hierarchical' => true,
				'supports' => array(
								'title', 
								'editor',   
								'page-attributes',
								),
				'query_var' => true,
				'taxonomies' => array('category'),
				'exclude_from_search' => false,
				);
				
	register_post_type( 'ucm_staff', $args);
	
	$ucm_events_labels = array(
				'name'               => 'Events',
				'singular_name'      => 'Event',
				'add_new'            => 'Add Event',
				'add_new_item'       => 'Add New Event',
				'edit_item'          => 'Edit Event',
				'new_item'           => 'New Event',
				'all_items'          => 'All Events',
				'view_item'          => 'View Event',
				'search_items'       => 'Search Events',
				'not_found'          => 'No events found',
				'not_found_in_trash' => 'No events found in Trash',
				'parent_item_colon'  => '',
				'menu_name'          => 'Events'
			);
			
	$args['labels'] = $ucm_events_labels;
	register_post_type( 'ucm_events', $args);
	
	$ucm_media_labels = array(
				'name'               => 'UCM Media',
				'singular_name'      => 'UCM Media',
				'add_new'            => 'Add UCM Media',
				'add_new_item'       => 'Add New UCM Media',
				'edit_item'          => 'Edit UCM Media',
				'new_item'           => 'New UCM Media',
				'all_items'          => 'All UCM Media',
				'view_item'          => 'View UCM Media',
				'search_items'       => 'Search UCM Media',
				'not_found'          => 'No UCM media found',
				'not_found_in_trash' => 'No UCM media found in Trash',
				'parent_item_colon'  => '',
				'menu_name'          => 'UCM Media'
			);	
			
	$ucm_media_args = $args;
	$ucm_media_args['labels'] = $ucm_media_labels;
	unset($ucm_media_args['taxonomies']);
	register_post_type( 'ucm_media', $ucm_media_args);
	
	$ucm_ministry_labels = array(
				'name'               => 'UCM Ministry',
				'singular_name'      => 'UCM Ministry',
				'add_new'            => 'Add UCM Ministry',
				'add_new_item'       => 'Add New UCM Ministry',
				'edit_item'          => 'Edit UCM Ministry',
				'new_item'           => 'New UCM Ministry',
				'all_items'          => 'All UCM Ministry',
				'view_item'          => 'View UCM Ministry',
				'search_items'       => 'Search UCM Ministry',
				'not_found'          => 'No UCM ministry found',
				'not_found_in_trash' => 'No UCM ministry found in Trash',
				'parent_item_colon'  => '',
				'menu_name'          => 'UCM Ministry'
			);	
			
	$args['labels'] = $ucm_ministry_labels;
	register_post_type( 'ucm_ministry', $args);
	
	$ucm_discipleship_labels = array(
				'name'               => 'UCM Discipleship',
				'singular_name'      => 'UCM Discipleship',
				'add_new'            => 'Add UCM Discipleship',
				'add_new_item'       => 'Add New UCM Discipleship',
				'edit_item'          => 'Edit UCM Discipleship',
				'new_item'           => 'New UCM Discipleship',
				'all_items'          => 'All UCM Discipleship',
				'view_item'          => 'View UCM Discipleship',
				'search_items'       => 'Search UCM Discipleship',
				'not_found'          => 'No UCM discipleship found',
				'not_found_in_trash' => 'No UCM discipleship found in Trash',
				'parent_item_colon'  => '',
				'menu_name'          => 'UCM Discipleship'
			);	
			
	$ucm_discipleship_args = $args;
	$ucm_discipleship_args['labels'] = $ucm_discipleship_labels;
	unset($ucm_discipleship_args['taxonomies']);
	register_post_type( 'ucm_discipleship', $ucm_discipleship_args);
	
	$ucm_slider_labels = array(
				'name'               => 'UCM Slider',
				'singular_name'      => 'UCM Slider',
				'add_new'            => 'Add UCM Slider',
				'add_new_item'       => 'Add New UCM Slider',
				'edit_item'          => 'Edit UCM Slider',
				'new_item'           => 'New UCM Slider',
				'all_items'          => 'All UCM Slider',
				'view_item'          => 'View UCM Slider',
				'search_items'       => 'Search UCM Slider',
				'not_found'          => 'No UCM Slider found',
				'not_found_in_trash' => 'No UCM Slider found in Trash',
				'parent_item_colon'  => '',
				'menu_name'          => 'UCM Slider'
			);	
			
	$ucm_slider_args = $args;
	$ucm_slider_args['labels'] = $ucm_slider_labels;
	register_post_type( 'ucm_slider', $ucm_slider_args);
	
}

function get_top_parent_page_id() {
 
    global $post;
 
    $ancestors = $post->ancestors;
 
    // Check if page is a child page (any level)
    if ($ancestors) {
 
        //  Grab the ID of top-level page from the tree
        return end($ancestors);
 
    } else {
 
        // Page is the top level, so use  it's own id
        return $post->ID;
 
    }
}

function limit_content($limit) {
	$content = strip_tags(get_the_content());
	$content = explode(' ', $content, $limit);
	
	if(count($content) >= $limit){
		array_pop($content);
		$content = implode(" ", $content).'...';
	} 
	else{
		$content = implode(" ",$content);
	}	
	
	// $content = preg_replace('/\[.+\]/','', $content);
	// $content = apply_filters('the_content', $content); 
	// $content = str_replace(']]>', ']]&gt;', $content);
	return $content;
}

add_action('after_setup_theme', 'blankslate_setup');
function blankslate_setup(){
load_theme_textdomain('blankslate', get_template_directory() . '/languages');
add_theme_support( 'automatic-feed-links' );
add_theme_support( 'post-thumbnails' );
global $content_width;
if ( ! isset( $content_width ) ) $content_width = 640;
register_nav_menus(
array( 'main-menu' => __( 'Main Menu', 'blankslate' ) )
);
}
add_action('comment_form_before', 'blankslate_enqueue_comment_reply_script');
function blankslate_enqueue_comment_reply_script()
{
if(get_option('thread_comments')) { wp_enqueue_script('comment-reply'); }
}
add_filter('the_title', 'blankslate_title');
function blankslate_title($title) {
if ($title == '') {
return 'Untitled';
} else {
return $title;
}
}
add_filter('wp_title', 'blankslate_filter_wp_title');
function blankslate_filter_wp_title($title)
{
return $title . esc_attr(get_bloginfo('name'));
}
add_filter('comment_form_defaults', 'blankslate_comment_form_defaults');
function blankslate_comment_form_defaults( $args )
{
$req = get_option( 'require_name_email' );
$required_text = sprintf( ' ' . __('Required fields are marked %s', 'blankslate'), '<span class="required">*</span>' );
$args['comment_notes_before'] = '<p class="comment-notes">' . __('Your email is kept private.', 'blankslate') . ( $req ? $required_text : '' ) . '</p>';
$args['title_reply'] = __('Post a Comment', 'blankslate');
$args['title_reply_to'] = __('Post a Reply to %s', 'blankslate');
return $args;
}
add_action( 'init', 'blankslate_add_shortcodes' );
function blankslate_add_shortcodes() {
add_shortcode('wp_caption', 'fixed_img_caption_shortcode');
add_shortcode('caption', 'fixed_img_caption_shortcode');
add_filter('img_caption_shortcode', 'blankslate_img_caption_shortcode_filter',10,3);
add_filter('widget_text', 'do_shortcode');
}
function blankslate_img_caption_shortcode_filter($val, $attr, $content = null)
{
extract(shortcode_atts(array(
'id'	=> '',
'align'	=> '',
'width'	=> '',
'caption' => ''
), $attr));
if ( 1 > (int) $width || empty($caption) )
return $val;
$capid = '';
if ( $id ) {
$id = esc_attr($id);
$capid = 'id="figcaption_'. $id . '" ';
$id = 'id="' . $id . '" aria-labelledby="figcaption_' . $id . '" ';
}
return '<figure ' . $id . 'class="wp-caption ' . esc_attr($align) . '" style="width: '
. (10 + (int) $width) . 'px">' . do_shortcode( $content ) . '<figcaption ' . $capid 
. 'class="wp-caption-text">' . $caption . '</figcaption></figure>';
}
add_action( 'widgets_init', 'blankslate_widgets_init' );
function blankslate_widgets_init() {
register_sidebar( array (
'name' => __('Sidebar Widget Area', 'blankslate'),
'id' => 'primary-widget-area',
'before_widget' => '<li id="%1$s" class="widget-container %2$s">',
'after_widget' => "</li>",
'before_title' => '<h3 class="widget-title">',
'after_title' => '</h3>',
) );
}
$preset_widgets = array (
'primary-aside'  => array( 'search', 'pages', 'categories', 'archives' ),
);
function blankslate_get_page_number() {
if (get_query_var('paged')) {
print ' | ' . __( 'Page ' , 'blankslate') . get_query_var('paged');
}
}
function blankslate_catz($glue) {
$current_cat = single_cat_title( '', false );
$separator = "\n";
$cats = explode( $separator, get_the_category_list($separator) );
foreach ( $cats as $i => $str ) {
if ( strstr( $str, ">$current_cat<" ) ) {
unset($cats[$i]);
break;
}
}
if ( empty($cats) )
return false;
return trim(join( $glue, $cats ));
}
function blankslate_tag_it($glue) {
$current_tag = single_tag_title( '', '',  false );
$separator = "\n";
$tags = explode( $separator, get_the_tag_list( "", "$separator", "" ) );
foreach ( $tags as $i => $str ) {
if ( strstr( $str, ">$current_tag<" ) ) {
unset($tags[$i]);
break;
}
}
if ( empty($tags) )
return false;
return trim(join( $glue, $tags ));
}
function blankslate_commenter_link() {
$commenter = get_comment_author_link();
if ( ereg( '<a[^>]* class=[^>]+>', $commenter ) ) {
$commenter = preg_replace( '/(<a[^>]* class=[\'"]?)/', '\\1url ' , $commenter );
} else {
$commenter = preg_replace( '/(<a )/', '\\1class="url "' , $commenter );
}
$avatar_email = get_comment_author_email();
$avatar = str_replace( "class='avatar", "class='photo avatar", get_avatar( $avatar_email, 80 ) );
echo $avatar . ' <span class="fn n">' . $commenter . '</span>';
}
function blankslate_custom_comments($comment, $args, $depth) {
$GLOBALS['comment'] = $comment;
$GLOBALS['comment_depth'] = $depth;
?>
<li id="comment-<?php comment_ID() ?>" <?php comment_class() ?>>
<div class="comment-author vcard"><?php blankslate_commenter_link() ?></div>
<div class="comment-meta"><?php printf(__('Posted %1$s at %2$s', 'blankslate' ), get_comment_date(), get_comment_time() ); ?><span class="meta-sep"> | </span> <a href="#comment-<?php echo get_comment_ID(); ?>" title="<?php _e('Permalink to this comment', 'blankslate' ); ?>"><?php _e('Permalink', 'blankslate' ); ?></a>
<?php edit_comment_link(__('Edit', 'blankslate'), ' <span class="meta-sep"> | </span> <span class="edit-link">', '</span>'); ?></div>
<?php if ($comment->comment_approved == '0') { echo '\t\t\t\t\t<span class="unapproved">'; _e('Your comment is awaiting moderation.', 'blankslate'); echo '</span>\n'; } ?>
<div class="comment-content">
<?php comment_text() ?>
</div>
<?php
if($args['type'] == 'all' || get_comment_type() == 'comment') :
comment_reply_link(array_merge($args, array(
'reply_text' => __('Reply','blankslate'),
'login_text' => __('Login to reply.', 'blankslate'),
'depth' => $depth,
'before' => '<div class="comment-reply-link">',
'after' => '</div>'
)));
endif;
?>
<?php }
function blankslate_custom_pings($comment, $args, $depth) {
$GLOBALS['comment'] = $comment;
?>
<li id="comment-<?php comment_ID() ?>" <?php comment_class() ?>>
<div class="comment-author"><?php printf(__('By %1$s on %2$s at %3$s', 'blankslate'),
get_comment_author_link(),
get_comment_date(),
get_comment_time() );
edit_comment_link(__('Edit', 'blankslate'), ' <span class="meta-sep"> | </span> <span class="edit-link">', '</span>'); ?></div>
<?php if ($comment->comment_approved == '0') { echo '\t\t\t\t\t<span class="unapproved">'; _e('Your trackback is awaiting moderation.', 'blankslate'); echo '</span>\n'; } ?>
<div class="comment-content">
<?php comment_text() ?>
</div>
<?php }