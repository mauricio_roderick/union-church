<!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta name="keywords" content="buy business online, sell business online, sold business online">
<meta name="description" content="I Sold My Business keeps you updated of the latest trends and teaches you ">
<meta http-equiv="content-type" content="<?php bloginfo('html_type'); ?>; charset=<?php bloginfo('charset'); ?>" />
<title>
	<?php wp_title(' - ', true, 'right'); ?>
	<?php echo (is_home()) ? ' - Blog for Buying & Selling Business Online' : ''; ?>
</title>
<link rel="stylesheet" type="text/css" href="<?php echo get_stylesheet_uri(); ?>" />
<?php wp_head(); ?>
</head>
<body <?php body_class(); ?>>
<div class="container">
	<div class="header">
		<ul class="pre_header">
			<li>
				<form action="<?php echo site_url() ?>" method="get" class="search_form" >
					<input type="text" name="s" placeholder="Search" class="field" />
					<input type="submit" name="" value="" class="submit" />
				</form>
			</li>
			<li><a href="<?php echo site_url('contact-us') ?>">Contact Us</a></li>
			<li><?php echo get_give_link() ?></li>
		</ul>
		<ul class="smb pull-right">
			<li><a href="<?php echo site_url() ?>"><img src="<?php echo get_template_directory_uri().'/images/fb.png' ?>" /></a></li>
			<li><a href="<?php echo site_url() ?>"><img src="<?php echo get_template_directory_uri().'/images/twitter.png' ?>" /></a></li>
			<li><a href="<?php echo site_url() ?>"><img src="<?php echo get_template_directory_uri().'/images/youtube.png' ?>" /></a></li>
			<li><a href="<?php echo site_url() ?>"><img src="<?php echo get_template_directory_uri().'/images/rss.png' ?>" /></a></li>
		</ul>
		<a class="logo" href="<?php echo site_url() ?>"><img src="<?php echo get_template_directory_uri().'/images/logo_.png' ?>" /></a>
		<div class="nav_">
			<ul>
				<li><a href="<?php echo site_url('get-to-know-us') ?>">Get to Know Us</a></li>
				<li><a href="<?php echo site_url('worship-services') ?>">Worship Services</a></li>
				<li><a href="<?php echo site_url('media') ?>">Sermons</a></li>
				<li><a href="<?php echo site_url('small-discipleship-groups') ?>">Small Discipleship Groups</a></li>
				<li><a href="<?php echo site_url('calendar') ?>">Events</a></li>
				<li><a href="<?php echo site_url('ministries') ?>">Ministries</a></li>
			</ul>
		</div>
	</div>
</div>