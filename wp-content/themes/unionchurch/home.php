<?php get_header(); ?>

<div class="container content" >
	
	<?php 
		get_template_part('ucm', 'home-slider');
		
		$home_page = get_page_by_title('home');
		
		echo($home_page) ? apply_filters('the_content', $home_page->post_content) : '';
	?>
</div>

<?php get_footer(); ?>