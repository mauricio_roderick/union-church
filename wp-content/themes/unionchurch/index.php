<?php get_header(); ?>

<div class="container content">
	<?php 
		$home_page = get_page_by_title('home');
		
		echo $home_page->post_content;
	?>
</div>

<?php get_footer(); ?>