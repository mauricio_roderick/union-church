
var markersArray = [];
var sd_group = [];
var global_map = [];
 
var $slider_cont = jQuery('.slider');
var $slide_left = jQuery('.slide_nav.left_');
var $slide_right = jQuery('.slide_nav.right_');

jQuery(function($) {

/*
*  render_map
*
*  This function will render a Google Map onto the selected jQuery element
*
*  @type	function
*  @date	8/11/2013
*  @since	4.3.0
*
*  @param	$el (jQuery element)
*  @return	n/a
*/
 
function render_map( $el ) {
 
	// var
	var $markers = $el.find('.marker');
 
	// vars
	var args = {
		zoom		: 16,
		center		: new google.maps.LatLng(0, 0),
		mapTypeId	: google.maps.MapTypeId.ROADMAP
	};
 
	// create map	        	
	var map = global_map = new google.maps.Map( $el[0], args);
 
	// add a markers reference
	map.markers = [];
 
	// add markers
	/* 
	$markers.each(function(){
 
    	add_marker( $(this), map );
 
	});
	*/
		
	add_markers(map)
	// center map
	center_map( map );
 
}
 
/*
*  add_marker
*
*  This function will add a marker to the selected Google Map
*
*  @type	function
*  @date	8/11/2013
*  @since	4.3.0
*
*  @param	$marker (jQuery element)
*  @param	map (Google Map object)
*  @return	n/a
*/
 
function add_markers( map ) {

	for(i in sd_group) {
		// var
		var latlng = new google.maps.LatLng( sd_group[i].lat, sd_group[i].lng );
	 
		// create marker
		var marker = new google.maps.Marker({
			position	: latlng,
			map			: map
		});
		// add to array
		map.markers.push( marker );
		markersArray.push(marker);
	}
 
}
 
/*
*  center_map
*
*  This function will center the map, showing all markers attached to this map
*
*  @type	function
*  @date	8/11/2013
*  @since	4.3.0
*
*  @param	map (Google Map object)
*  @return	n/a
*/
 
function center_map( map ) {
 
	// vars
	var bounds = new google.maps.LatLngBounds();
 
	// loop through all markers and create bounds
	$.each( map.markers, function( i, marker ){
 
		var latlng = new google.maps.LatLng( marker.position.lat(), marker.position.lng() );
 
		bounds.extend( latlng );
 
	});
 
	// only 1 marker?
	if( map.markers.length == 1 )
	{
		// set center of map
	    map.setCenter( bounds.getCenter() );
	    map.setZoom( 16 );
	}
	else
	{
		// fit to bounds
		map.fitBounds( bounds );
	}
 
}
 
 
function show_location(e)
{
	var index = e.attr('data-index');
	
	if(typeof sd_group[index] != 'undefined')
	{
		clearMap();
		
		var latlng = new google.maps.LatLng( sd_group[index].lat, sd_group[index].lng );
		var marker = new google.maps.Marker({
			position	: latlng,
			map			: global_map
		});
	
		global_map.markers.push( marker );
		markersArray.push(marker);
		center_map(global_map)
		
		var active_sdg_item = jQuery('.item_:eq(' + index + ')', $slider_cont);
		active_sdg_item.addClass('show active');
	}
	else
	{
		alert('false')
	}
}

function show_all_markers()
{
	clearMap();
	
	$.each( sd_group, function( index, value ){
	
		var latlng = new google.maps.LatLng( sd_group[index].lat, sd_group[index].lng );
		var marker = new google.maps.Marker({
			position	: latlng,
			map			: global_map
		});
	
		global_map.markers.push( marker );
		markersArray.push(marker);
		
		google.maps.event.addListener(marker, 'click', (function(marker){
			return show_sdg_item(index);
		}));
		
	});
	
	center_map(global_map);
}

function show_sdg_item(index){
	jQuery('.item_', $slider_cont).removeClass('show active');
	
	var last_visible = jQuery('.item_:eq(' + index + ')', $slider_cont);
	last_visible.addClass('show active');
	
	next_item = last_visible.next('.item_')
	if(next_item)
	next_item.addClass('show');
}

function clearMap(){
	for(i in markersArray) {
		markersArray[i].setMap();
	}
	markersArray = [];
	
	jQuery('.item_', $slider_cont).removeClass('active');
}
/*
*  document ready
*
*  This function will render each map when the document is ready (page has loaded)
*
*  @type	function
*  @date	8/11/2013
*  @since	5.0.0
*
*  @param	n/a
*  @return	n/a
*/

$slide_right.click(function(){
	var next_item = false;
	var last_visible = jQuery('.show', $slider_cont).last();
	jQuery('.item_', $slider_cont).removeClass('show active');
	
	next_item = last_visible.next('.item_')
	if(next_item.index() >= 0)
	{
		next_item.addClass('show');
		last_visible = next_item;
	}
	else
	{
		var first_item = jQuery('.item_', $slider_cont).first();
		first_item.addClass('show');
		last_visible = first_item;
	}
	
	next_item = last_visible.next('.item_');
	if(next_item)
	next_item.addClass('show');
	
})

$slide_left.click(function(){
	var next_item = false;
	var first_visible = jQuery('.show', $slider_cont).first();
	jQuery('.item_', $slider_cont).removeClass('show active');
	
	next_item = first_visible.prev('.item_')
	if(next_item.index() >= 0)
	{
		next_item.addClass('show');
		first_visible = next_item;
	}
	else
	{
		var last_item = jQuery('.item_', $slider_cont).last();
		last_item.addClass('show');
		first_visible = last_item;
	}
	
	next_item = first_visible.prev('.item_')
	if(next_item)
	next_item.addClass('show');
	
})

$('.show_map', $slider_cont).click(function(){
	show_location( $(this) );
});

$('.show_all_markers').click(function(){
	show_all_markers();
});

$('.ucm_sd_map').each(function(){
	render_map( $(this) );
	show_all_markers();
});

 
	
})