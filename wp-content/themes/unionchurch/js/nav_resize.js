jQuery(document).ready(function(){
	function nav_resize()
	{
		var page_content_height = jQuery('.content_container').outerHeight(true);
		var nav_height = jQuery('.ucp_page .nav_').outerHeight(true);
		
		if(page_content_height > nav_height)
		jQuery('.ucp_page .nav_').css('height', page_content_height);
	}
	
	setTimeout(nav_resize, 2000);
});