<?php
/*
Template Name: Page Temp with Left Nav
*/
?>

<?php get_header(); ?>
<div id="post-<?php the_ID(); ?>" class="container mcp_page">
	<?php 
		if( have_posts() )
		{
			while( have_posts() )
			{
				the_post()
	?>
	<div class="row-fluid">
		<div class="span3">
			<div class="nav_top"></div>
		</div>
		<div class="span9 banner">
			<?php if ( has_post_thumbnail() ) { the_post_thumbnail(); } ?>
		</div>
	</div>
	<div class="clear_both spacing"></div>
	<div class="row-fluid">
		<div class="span3">
			<?php get_template_part('mcp', 'left-nav'); ?>
		</div>
		<div class="span9 content_container">
			<div class="content">
				<?php //edit_post_link(); ?>
				<?php apply_filters('the_content', the_content()) ?>
			</div>
		</div>
	</div>
	<div class="row-fluid border_group">
		<div class="span3">
			<div class="border_1"></div>
		</div>
		<div class="span3">
			<div class="border_2"></div>
		</div>
		<div class="span3">
			<div class="border_3"></div>
		</div>
		<div class="span3">
			<div class="border_4"></div>
		</div>
	</div>
	<?php
			}
		}
	?>
</div>

<?php get_footer(); ?>