<?php get_header(); ?>

<div id="post-<?php the_ID(); ?>" class="container ucp_page media index">
	<?php 
		if( have_posts() )
		{
			while( have_posts() )
			{
				the_post()
	?>
	<div class="row-fluid">
		<div class="span3">
			<div class="nav_top"></div>
		</div>
		<div class="span9">
			<div class="banner">
			<div class="ucp_main_title1"><?php echo the_title() ?></div>
			<?php 
				if ( has_post_thumbnail() )
				{
					the_post_thumbnail();
				}
				else // show default banner
				{
					echo '<img src="'. get_template_directory_uri().'/images/ucm_media.jpg" />';
				}
			?>
			</div>
		</div>
	</div>
	<div class="clear_both spacing"></div>
	<div class="media_content dsp_table">
		<div class="tbl_cell video">
			<?php
			
			$args = array(
						'orderby' => 'post_date',
						'order' => 'ASC',
						'post_type' => 'ucm_media',
						'category__in' => array(8),
						'post_status' => 'publish',
						'posts_per_page' => 4,
						);
						
			$video_sermons = new WP_Query( $args );
		
			if($video_sermons->found_posts > 0)
			{ 
				$post = $video_sermons->posts[0];
				setup_postdata($post);
			?>
				<div class="heading"><span>(VIDEO)</span> LATEST SUNDAY SERMON</div>
				<div class="latest">
					<?php 
						$video_url = get_field('ucm_mv_id');
						$video_url = explode('/', $video_url);
						$video_url = end($video_url);
						$video_url = "//player.vimeo.com/video/{$video_url}?portrait=0&amp;badge=0&amp;byline=0&amp;title=0";
					?>
					<iframe src="<?php echo $video_url ?>" width="100%" height="350px" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>
					<div class="dtls">
						<?php echo get_the_date('D, M d, Y') ?>
						<a href="<?php echo get_permalink() ?>" class="title_"><?php echo the_title() ?></a>
						<div class="sub_title"><?php echo the_field('ucm_mv_sub_title') ?></div>
					</div>
				</div>
			<?php
				if($video_sermons->found_posts > 1)
				{
			?>
				<div class="heading"><span>(VIDEO)</span> PREVIOUS SUNDAY SERMONS</div>
			<?php
					$previous_sermons = $video_sermons->posts;
					unset($previous_sermons[0]);
					
					foreach($previous_sermons as $post)
					{
						setup_postdata($post);
						$image = get_field('ucm_mv_image');
						$image_src = ($image) ? $image['sizes']['thumbnail'] : get_template_directory_uri().'/images/img_sub.png'
			?>
				<div class="item dsp_table">
					<div class="image tbl_cell">
						<img src="<?php echo $image_src ?>" />
					</div>
					<div class="dtls tbl_cell">
						<?php echo get_the_date('D, M d, Y') ?>
						<a href="<?php echo get_permalink() ?>" class="title_"><?php echo the_title() ?></a>
						<div class="sub_title"><?php echo the_field('ucm_mv_sub_title') ?></div>
						<div class="btn_cont">
							<a href="<?php echo get_permalink() ?>" >WATCH</a>
						</div>
					</div>
				</div>
			<?php
					}
				}
			}
			?>
			<p class="view_all">
				<a href="<?php echo site_url('media/sermon-videos') ?>" >View All Video Sermon</a>
			</p>
		</div>
		<div class="tbl_cell audio">
			<?php
			
			$args = array(
						'orderby' => 'post_date',
						'order' => 'ASC',
						'post_type' => 'ucm_media',
						'category__in' => array(9),
						'post_status' => 'publish',
						'posts_per_page' => 6,
						);
						
			$audio_sermons = new WP_Query( $args );
		
			if($audio_sermons->found_posts > 0)
			{ 
				$post = $audio_sermons->posts[0];
				setup_postdata($post);
			?>
				<div class="heading"><span>(AUDIO)</span> LATEST SUNDAY SERMON</div>
				<div class="latest">
					<div class="item">
						<?php 
							$audio = get_field('ucm_ma_file');
							//print_r($audio);
							if($audio){
						?>
						<audio preload="auto" controls>
							<source src="<?php echo $audio['url'] ?>">
						</audio>
						<?php } ?>
						<div class="dtls">
							<?php echo get_the_date('D, M d, Y') ?>
							<a href="<?php echo get_permalink() ?>" class="title_"><?php echo the_title() ?></a>
							<div class="sub_title"><?php echo the_field('ucm_ma_sub_title') ?></div>
						</div>
					</div>
				</div>
			<?php	
				if($audio_sermons->found_posts > 1)
				{
			?>
				<div class="heading"><span>(AUDIO)</span> PREVIOUS SUNDAY SERMONS</div>
			<?php
					$previous_sermons = $audio_sermons->posts;
					unset($previous_sermons[0]);
					
					foreach($previous_sermons as $post)
					{
						setup_postdata($post);
			?>
				<div class="item">
					<div class="dtls">
						<?php echo get_the_date('D, M d, Y') ?>
						<a href="<?php echo get_permalink() ?>" class="title_"><?php echo the_title() ?></a>
						<div class="sub_title"><?php echo the_field('ucm_ma_sub_title') ?></div>
						<div class="btn_cont">
							<a href="<?php echo get_permalink().'?download=1' ?>" >DOWNLOAD</a>
							<a href="<?php echo get_permalink() ?>" >PLAY</a>
						</div>
					</div>
				</div>
			<?php
					}
				}
			}
			?>
			<p class="view_all">
				<a href="<?php echo site_url('media/sermon-mp3s') ?>" class="" >View All Audio Sermon</a>
			</p>
		</div>
	</div>
	
	<div class="row-fluid border_group">
		<div class="span3">
			<div class="border_3"></div>
		</div>
		<div class="span3">
			<div class="border_1"></div>
		</div>
		<div class="span3">
			<div class="border_2"></div>
		</div>
		<div class="span3">
			<div class="border_4"></div>
		</div>
	</div>
	<?php
			}
		}
	?>
</div>

<link rel="stylesheet" href="<?php echo get_template_directory_uri().'/css/audioplayer.css'; ?>" />
<script src="<?php echo get_template_directory_uri().'/js/audioplayer.min.js'; ?>"></script>
<script>jQuery( function($) { $( 'audio' ).audioPlayer(); } );</script>
<?php get_footer(); ?>