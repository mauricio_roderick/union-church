<?php get_header(); ?>

<div class="container content">
	<?php the_post(); ?>
	<div id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
		<?php the_content() ?>
	</div>
</div>

<?php //get_sidebar(); ?>
<?php get_footer(); ?>