<?php get_header(); ?>
<div class="container ucp_page search">
	<?php if ( have_posts() ){ ?>
		<h3 class="page-title"><?php printf( __( 'Search Results for: %s', 'blankslate' ), '<span>' . get_search_query()  . '</span>' ); ?></h3>
		<?php //get_template_part( 'nav', 'above' ); ?>
		<?php while ( have_posts() ) : the_post() ?>
		<?php get_template_part( 'entry' ); ?>
		<?php endwhile; ?>
		<?php get_template_part( 'nav', 'below' ); ?>
	<?php }else{ ?>
		<div id="post-0" class="post no-results not-found">
			<h3 class="entry-title"><?php _e( 'Nothing Found', 'blankslate' ) ?></h3>
			<div class="entry-content">
			<p><?php _e( 'Sorry, nothing matched your search. Please try again.', 'blankslate' ); ?></p>
			</div>
		</div>
	<?php } ?>
</div>
<?php get_footer(); ?>