<?php get_header(); ?>
<?php the_post() ?>

<div id="post-<?php the_ID(); ?>" class="container ucp_page events single">
	<div class="row-fluid">
		<div class="span3">
			<div class="nav_top"></div>
			<div class="spacing"></div>
		</div>
		<div class="span9">
			<div class="banner">
			<div class="ucp_main_title1">Events</div>
			<?php 
				if ( has_post_thumbnail() )
				{
					the_post_thumbnail();
				}
				else // show default banner
				{
					echo '<img src="'. get_template_directory_uri().'/images/events_banner.jpg" />';
				}
			?>
			</div>
		</div>
	</div>
	
	<div class="row-fluid border_group">
		<div class="span3">
			<?php get_template_part('ucm-events', 'left-nav'); ?>
		</div>
		<div class="span9">
			<div class="content_container">
				<div class="content">
					<div class="name"><?php echo the_title() ?></div>
					<div class="date">
						<?php 
							$date = get_field('ucm_event_date');
							
							if($date)
							{
								$date = strtotime($date);
								echo date('D, M d, Y', $date);
							}
						?>
					</div>
					<?php apply_filters('the_content', the_content()) ?>
				</div>
			</div>
		</div>
	</div>
	
	<div class="row-fluid border_group">
		<div class="span3">
			<div class="border_2"></div>
		</div>
		<div class="span3">
			<div class="border_1"></div>
		</div>
		<div class="span3">
			<div class="border_3"></div>
		</div>
		<div class="span3">
			<div class="border_4"></div>
		</div>
	</div>
</div>
<script type="text/javascript" src="<?php echo get_template_directory_uri().'/js/nav_resize.js'; ?>"></script>

<?php get_footer(); ?>