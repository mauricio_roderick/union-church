
<?php 

ucm_force_download();
get_header(); 
the_post();

?>

<div id="post-<?php the_ID(); ?>" class="container ucp_page media single">
	<div class="row-fluid">
		<div class="span3">
			<div class="nav_top"></div>
			<div class="spacing"></div>
		</div>
		<div class="span9">
			<div class="banner">
			<div class="ucp_main_title1">Media</div>
			<?php 
				if ( has_post_thumbnail() )
				{
					the_post_thumbnail();
				}
				else // show default banner
				{
					echo '<img src="'. get_template_directory_uri().'/images/ucm_sermons.jpg" />';
				}
			?>
			</div>
		</div>
	</div>
	
	<div class="row-fluid border_group">
		<div class="span3">
			<?php get_template_part('ucm-media', 'left-nav'); ?>
		</div>
		<div class="span9">
			<div class="content_container">
				<div class="content">
					<?php 
						$watch_mode = isset($_GET['watch']);
						
						get_template_part('ucm-media', 'item-view')
					?>
				</div>
			</div>
		</div>
	</div>
	
	<div class="row-fluid border_group">
		<div class="span3">
			<div class="border_3"></div>
		</div>
		<div class="span3">
			<div class="border_1"></div>
		</div>
		<div class="span3">
			<div class="border_2"></div>
		</div>
		<div class="span3">
			<div class="border_4"></div>
		</div>
	</div>
</div>
<script type="text/javascript" src="<?php echo get_template_directory_uri().'/js/nav_resize.js'; ?>"></script>

<?php get_footer(); ?>