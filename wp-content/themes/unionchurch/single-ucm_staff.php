
<?php get_header(); ?>

<div id="post-<?php the_ID(); ?>" class="container ucp_page gtku single_staff">
	<?php 
		if( have_posts() )
		{
			while( have_posts() )
			{
				the_post()
	?>
	<div class="row-fluid">
		<div class="span3">
			<?php get_template_part('ucm', 'staff-left-nav'); ?>
		</div>
		
		<div class="span9">
			<div class="banner">
			<div class="ucp_main_title1">Our Staff</div>
			<?php 
				if ( has_post_thumbnail() )
				{
					the_post_thumbnail();
				}
				else // show default banner
				{
					echo '<img src="'. get_template_directory_uri().'/images/ucm_our_staff.jpg" />';
				}
			?>
			</div>
			<div class="spacing"></div>
			<div class="content_container">
			<div class="content">
				<div class="item">
				<div class="_name"><?php echo the_title() ?></div>
				<div class="_title"><?php echo the_field('staff_title') ?></div>
				
				<?php 
					$image = get_field('staff_image');
					if($image)
					{
						$image_src = $image['sizes']['medium'];
				?>
					<img class="staff_image img-circle" src="<?php echo $image_src ?>" />
				<?php } ?>
				<?php apply_filters('the_content', the_content()) ?>
			</div>
			</div>
		</div>
	</div>
	<?php
			}
		}
	?>
</div>
<div class="row-fluid border_group">
	<div class="span3">
		<div class="border_1"></div>
	</div>
	<div class="span3">
		<div class="border_2"></div>
	</div>
	<div class="span3">
		<div class="border_3"></div>
	</div>
	<div class="span3">
		<div class="border_4"></div>
	</div>
</div>
<script type="text/javascript" src="<?php echo get_template_directory_uri().'/js/nav_resize.js'; ?>"></script>

<?php get_footer(); ?>