<?php get_header(); ?>
<div class="container">
<?php 
	if ( have_posts() )
	{
		while ( have_posts() )
		{
			the_post();	
?>
	<div id="post-<?php the_ID(); ?>" <?php post_class(); ?> >
		<h3 class="title"><?php the_title(); ?></h3>
		<div class="date">Posted on <?php echo get_the_date('F j, Y'); ?></div>
		
		<?php get_template_part('entry','content') ?>
	</div> 
<?php 
		}
	}
	else
	{
		echo 'No posts found.';
	}
?>
</div>
<?php get_footer(); ?>