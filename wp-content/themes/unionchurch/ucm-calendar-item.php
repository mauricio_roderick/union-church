<?php

$trimmed_content = wp_trim_words( $post->post_content, 30, '...<a href="'. get_permalink() .'"> continue reading</a>' );
$date = strtotime(get_field('ucm_ministry_date'));
$day = date('j', $date);
$month = date('M', $date);

?>
<div class="item">
	<div class="date">
		<div class="day"><?php echo $day ?></div>
		<div class="month"><?php echo $month ?></div>
	</div>
	<div class="dtls">
		<div class="name"><?php echo the_title() ?></div>
		<div class="title"><?php echo the_field('ucm_ministry_sub_header') ?></div>
		<div class="intro"><?php echo $trimmed_content  ?></div>
	</div>
	<div class="clear_both"></div>
</div>