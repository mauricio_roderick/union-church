<?php
/*
Template Name: Contact Us
*/
?>

<?php get_header(); ?>

<div id="post-<?php the_ID(); ?>" class="container ucp_page contact_us">
	<?php 
		if( have_posts() )
		{
			while( have_posts() )
			{
				the_post()
	?>
	<div class="row-fluid">
		<div class="span">
			<div class="banner">
			<div class="ucp_main_title1"><?php echo the_title() ?></div>
			<?php 
				if ( has_post_thumbnail() )
				{
					the_post_thumbnail();
				}
				else // show default banner
				{
					echo '<img src="'. get_template_directory_uri().'/images/ucm_contact_us.jpg" />';
				}
			?>
			</div>
			<div class="spacing"></div>
		</div>
	</div>
	<div class="row-fluid">
		<?php apply_filters('the_content', the_content()) ?>
	</div>
	<?php
			}
		}
	?>
	
	
	<?php 

	$location = get_field('cu_map');
	 
	if( !empty($location) ):
	?>
	<style type="text/css">
	 
	.acf-map {
		width: 100%;
		height: 400px;
		border: #ccc solid 1px;
		margin: 20px 0;
	}
	 
	</style>
	<script type="text/javascript" src="<?php echo get_template_directory_uri().'/js/google_map.js'; ?>"></script>
	<script src="https://maps.googleapis.com/maps/api/js?v=3.exp&sensor=false"></script>
	<div class="ucm_map">
		<div class="marker" data-lat="<?php echo $location['lat']; ?>" data-lng="<?php echo $location['lng']; ?>"></div>
	</div>
	<?php endif; ?>
</div>

<?php get_footer(); ?>