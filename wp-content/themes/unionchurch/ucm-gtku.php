<?php
/*
Template Name: Get to know Us
*/
?>

<?php get_header(); ?>

<div id="post-<?php the_ID(); ?>" class="container ucp_page gtku">
	<?php 
		if( have_posts() )
		{
			while( have_posts() )
			{
				the_post()
	?>
	<div class="row-fluid">
		<div class="span3">
			<?php get_template_part('ucm', 'left-nav'); ?>
		</div>
		
		<div class="span9">
			<div class="banner">
			<div class="ucp_main_title1"><?php echo the_title() ?></div>
			<?php 
				if ( has_post_thumbnail() )
				{
					the_post_thumbnail();
				}
				else // show default banner
				{
					echo '<img src="'. get_template_directory_uri().'/images/ucm_get_to_know_us.jpg" />';
				}
			?>
			</div>
			<div class="spacing"></div>
			<div class="content_container">
				<div class="content">
					<?php apply_filters('the_content', the_content()) ?>
				</div>
			</div>
		</div>
	</div>
	<div class="row-fluid border_group">
		<div class="span3">
			<div class="border_1"></div>
		</div>
		<div class="span3">
			<div class="border_2"></div>
		</div>
		<div class="span3">
			<div class="border_3"></div>
		</div>
		<div class="span3">
			<div class="border_4"></div>
		</div>
	</div>
	<?php
			}
		}
	?>
</div>
<script type="text/javascript" src="<?php echo get_template_directory_uri().'/js/nav_resize.js'; ?>"></script>

<?php get_footer(); ?>