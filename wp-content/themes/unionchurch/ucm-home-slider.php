<?php
	$slide_params = array(
				'orderby' => 'menu_order',
				'order' => 'ASC',
				'post_type' => 'ucm_slider',
				'category__in' => array(),
				'post_status' => 'publish',
				);
				
	$slides = new WP_Query( $slide_params );
?>

<div class="home_banner">
	<div class="slider-wrapper theme-default">
		<div id="slider" class="nivoSlider">
		<?php
			foreach ( $slides->posts as $post ) 
			{
				setup_postdata($post);
				
				$image = get_field('ucm_slider_image');
				
				$image_src_relative = str_replace(site_url().'/', '',$image['url']);
				if($image && file_exists($image_src_relative))
				{
					$link = get_field('ucm_slider_link', $post->ID);
					$link = ($link != '') ? "href='$link' target='blank'" : '';
			?>
				<a <?php echo $link ?> ><img src="<?php echo $image['url'] ?>" alt="" /></a>
			<?php
			
				}
			}
		?>
		</div>
	</div>
</div>

<link rel="stylesheet" href="<?php echo get_template_directory_uri().'/nivo_slider/css/nivo-slider.css' ?>" type="text/css" media="screen" />
<link rel="stylesheet" href="<?php echo get_template_directory_uri().'/nivo_slider/css/default_nivo.css' ?>" type="text/css" media="screen" />
<script type="text/javascript" src="<?php echo get_template_directory_uri().'/nivo_slider/js/jquery.nivo.slider.pack.js' ?>"></script>
<script type="text/javascript">
jQuery(window).load(function() {
	jQuery('#slider').nivoSlider({
		effect: 'fade',            
		pauseTime: 5000,            
		directionNav: false,            
		controlNav: false,              
		controlNavThumbs: false, 
		pauseOnHover: false, 
		manualAdvance: false,
	});
});
</script>