<?php
/*
Template Name: Media Archive
*/
?>

<?php get_header(); ?>
<?php the_post() ?>

<div id="post-<?php the_ID(); ?>" class="container ucp_page media media_index">
	<div class="row-fluid">
		<div class="span3">
			<div class="nav_top"></div>
			<div class="spacing"></div>
		</div>
		<div class="span9">
			<div class="banner">
			<div class="ucp_main_title1"><?php echo the_title() ?></div>
			<?php 
				if ( has_post_thumbnail() )
				{
					the_post_thumbnail();
				}
				else // show default banner
				{
					echo '<img src="'. get_template_directory_uri().'/images/ucm_sermons.jpg" />';
				}
			?>
			</div>
		</div>
	</div>
	
	<div class="row-fluid border_group">
		<div class="span3">
			<?php get_template_part('ucm-media', 'left-nav'); ?>
			
		</div>
		<div class="span9">
			<div class="content_container">
				<?php
					
					$month = (get_query_var('m_month')) ? get_query_var('m_month') : '';
					$year = (get_query_var('m_year')) ? get_query_var('m_year') : '';
					$media_archive_slug = "ma-$month-$year";
					
					$args = array(
								'name' => $media_archive_slug,
								'post_type' => 'post',
								'post_status' => 'publish',
								'posts_per_page' => 1
							);
							
					$my_posts = get_posts( $args );
					if( $my_posts )
					echo apply_filters( 'the_content', $my_posts[0]->post_content );
				?>
				<div class="content listing">
					<?php
						$page = (get_query_var('page')) ? get_query_var('page') : 1;
						
						$args = array(
									'order' => 'DESC',
									'orderby' => 'post_date',
									'post_type'=> 'ucm_media',
									'post_status' => 'publish',
									'posts_per_page' => -1,
								);
								
						$m_month = '';
						$m_year = '';
						if(isset($_GET['m_month']) && isset($_GET['m_year']))
						{					
							$args['year'] = $m_year = $_GET['m_year'];
							$args['monthnum'] = $m_month = $_GET['m_month'];
						}
						
						$total_sermons = query_posts( $args );
						$posts_per_page = get_option('posts_per_page', 5);
						$total_page = ceil(count($total_sermons) / $posts_per_page);
							
						$args['posts_per_page'] = $posts_per_page;
						$args['paged'] = $page;
						
						$sermons = query_posts( $args );
						
						$paginate_links = paginate_links( 
															array(
															'base' => site_url("media-archive/%_%?m_month=$m_month&m_year=$m_year"),
															'format' => '%#%',
															'total' => $total_page,
															'current' => $page,
															'type' => 'list',
															)
														);
							
						foreach($sermons as $post)
						{
							setup_postdata($post);
							$image = get_field('ucm_sermon_image');
							
							$image_src = ($image) ? $image['sizes']['thumbnail'] : get_template_directory_uri().'/images/img_sub.png';
							$trimmed_content = wp_trim_words( $post->post_content, 75, '' );
							
							$audio = get_field('ucm_sermon_file');
						?>
							<div class="item dsp_table">
								<div class="image tbl_cell">
									<img src="<?php echo $image_src ?>" />
								</div>
								<div class="dtls tbl_cell">
									<?php echo get_the_date('D, M d, Y') ?>
									<div class="title_"><?php echo the_title() ?></div>
									<div class="sub_title"><?php echo the_field('ucm_sermon_sub_title') ?></div>
									<div class="intro"><?php echo $trimmed_content ?></div>
									<div class="btn_cont">
										<a href="<?php echo get_permalink() ?>" >VIEW</a>
										<?php if($audio){ ?>
										<a href="<?php echo get_permalink().'?download=1' ?>" >DOWNLOAD AUDIO</a>
										<?php } ?>
									</div>
								</div>
							</div>
					<?php 
						} 
					
						wp_reset_postdata();
						echo ucm_pagination($paginate_links);
					?>
					
				</div>
			</div>
		</div>
	</div>
	
	<div class="row-fluid border_group">
		<div class="span3">
			<div class="border_3"></div>
		</div>
		<div class="span3">
			<div class="border_1"></div>
		</div>
		<div class="span3">
			<div class="border_2"></div>
		</div>
		<div class="span3">
			<div class="border_4"></div>
		</div>
	</div>
</div>
<script type="text/javascript" src="<?php echo get_template_directory_uri().'/js/nav_resize.js'; ?>"></script>

<?php get_footer(); ?>