<div class="dtls">
	<div class="date"><?php echo get_the_date('D, M d, Y') ?></div>
	<div class="name"><?php echo the_title() ?></div>
	<div class="sub_title"><?php echo the_field('ucm_mv_sub_title') ?></div>
	<div class="sub_title"><?php echo the_field('ucm_sermon_sub_title') ?></div>
</div>
<?php 
	$watch_mode = isset($_GET['watch']);
	
	$video_url = get_field('ucm_sermon_video_id');
	if($video_url)
	{
		$video_url = explode('/', $video_url);
		$video_url = end($video_url);
		$video_url = "//player.vimeo.com/video/{$video_url}?portrait=0&amp;badge=0&amp;byline=0&amp;title=0"
?>
	<iframe src="<?php echo $video_url ?>" width="100%" height="350px" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>
<?php 
	}
	
	$audio = get_field('ucm_sermon_file');
	if($audio)
	{
?>
	<div class="audio_cont">
		<audio preload="auto" controls>
			<source src="<?php echo $audio['url'] ?>">
		</audio>
	</div>
	<div class="btn_cont">
		<a href="<?php echo get_permalink().'?download=1' ?>" >DOWNLOAD AUDIO</a>
	</div>
	
	<link rel="stylesheet" href="<?php echo get_template_directory_uri().'/css/audioplayer.css'; ?>" />
	<script src="<?php echo get_template_directory_uri().'/js/audioplayer.min.js'; ?>"></script>
	<script>jQuery( function($) { $( 'audio' ).audioPlayer(); } );</script>
<?php } ?>

<?php apply_filters('the_content', the_content()) ?>