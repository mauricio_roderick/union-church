
<div class="nav_">
		<?php 
			$media_archive = "SELECT post_date as archive_date FROM uc_posts WHERE post_type = 'ucm_media' and post_status = 'publish' GROUP BY DATE_FORMAT(post_date,'%Y %m') ORDER BY post_date DESC";
            $media_archive = $wpdb->get_results($media_archive);
			
			$navs = array();
			foreach($media_archive as $date)
			{
				$date_ = date('F', strtotime($date->archive_date));
				$month = date('m', strtotime($date->archive_date));
				$year = date('Y', strtotime($date->archive_date));
				
				$navs[$year][$month] = $date_;
			}
			
			foreach($navs as $year => $months)
			{
		?>
		
		<div class="item">
			<a class="parent"><?php echo 'Year '.$year ?></a>
			<div class="sub_nav">
				<?php foreach($months as $month_index => $month_label){ ?>
					<a href="<?php echo site_url("media-archive?m_month=$month_index&m_year=$year") ?>"><?php echo $month_label ?></a>
				<?php } ?>
			</div>
		</div>
		
		<?php 
			}
		?>
</div>