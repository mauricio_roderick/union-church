<?php
/*
Media - Video
*/
?>

<?php get_header(); ?>
<?php the_post() ?>

<div id="post-<?php the_ID(); ?>" class="container ucp_page media media_index">
	<div class="row-fluid">
		<div class="span3">
			<div class="nav_top"></div>
			<div class="spacing"></div>
		</div>
		<div class="span9">
			<div class="banner">
			<div class="ucp_main_title1"><?php echo the_title() ?></div>
			<?php 
				if ( has_post_thumbnail() )
				{
					the_post_thumbnail();
				}
				else // show default banner
				{
					echo '<img src="'. get_template_directory_uri().'/images/ucm_sermons.jpg" />';
				}
			?>
			</div>
		</div>
	</div>
	
	<div class="row-fluid border_group">
		<div class="span3">
			<?php get_template_part('ucm-media', 'left-nav'); ?>
		</div>
		<div class="span9">
			<div class="content_container">
				<div class="content listing">
					<?php
						$page = (get_query_var('page')) ? get_query_var('page') : 1;
						
						$args = array(
									'orderby' => 'post_date',
									'order' => 'ASC',
									'post_type' => 'ucm_media',
									'category__in' => array(8),
									'post_status' => 'publish',
									'posts_per_page' => get_option( 'posts_per_page', 5 ),
									'paged' => $page,
									);
									
						$video_sermons = new WP_Query( $args );
					
						$paginate_links = paginate_links( 
															array(
															'format' => '?page=%#%',
															'total' => $video_sermons->max_num_pages,
															'current' => $page,
															'type' => 'list',
															)
														);
														
						foreach($video_sermons->posts as $post)
						{
							setup_postdata($post);
							$image = get_field('ucm_mv_image');
							
							$image_src = ($image) ? $image['sizes']['thumbnail'] : get_template_directory_uri().'/images/img_sub.png';
							$trimmed_content = wp_trim_words( $post->post_content, 75, '' );
						?>
							<div class="item dsp_table">
								<div class="image tbl_cell">
									<img src="<?php echo $image_src ?>" />
								</div>
								<div class="dtls tbl_cell">
									<?php echo get_the_date('D, M d, Y') ?>
									<div class="title_"><?php echo the_title() ?></div>
									<div class="sub_title"><?php echo the_field('ucm_mv_sub_title') ?></div>
									<div class="intro"><?php echo $trimmed_content ?></div>
									<a href="<?php the_permalink() ?>" class="btn_">WATCH</a>
								</div>
							</div>
					<?php 
						} 
					
						wp_reset_postdata();
						echo ucm_pagination($paginate_links);
					?>
					
				</div>
			</div>
		</div>
	</div>
	
	<div class="row-fluid border_group">
		<div class="span3">
			<div class="border_3"></div>
		</div>
		<div class="span3">
			<div class="border_1"></div>
		</div>
		<div class="span3">
			<div class="border_2"></div>
		</div>
		<div class="span3">
			<div class="border_4"></div>
		</div>
	</div>
</div>
<script type="text/javascript" src="<?php echo get_template_directory_uri().'/js/nav_resize.js'; ?>"></script>

<?php get_footer(); ?>