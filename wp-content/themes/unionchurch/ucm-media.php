<?php get_header(); ?>

<div id="post-<?php the_ID(); ?>" class="container ucp_page media">
	<?php 
		if( have_posts() )
		{
			while( have_posts() )
			{
				the_post()
	?>
	<div class="row-fluid">
		<div class="span3">
			<div class="nav_top"></div>
		</div>
		<div class="span9">
			<div class="banner">
			<div class="ucp_main_title1"><?php echo the_title() ?></div>
			<?php 
				if ( has_post_thumbnail() )
				{
					the_post_thumbnail();
				}
				else // show default banner
				{
					echo '<img src="'. get_template_directory_uri().'/images/ucm_media.jpg" />';
				}
			?>
			</div>
		</div>
	</div>
	<div class="clear_both spacing"></div>
	<div class="media_content">
		<div class="tbl_cell video">
			<?php apply_filters('the_content', the_content()) ?>
		</div>
		<div class="tbl_cell audio">
		asd
		</div>
	</div>
	
	<div class="row-fluid border_group">
		<div class="span3">
			<div class="border_3"></div>
		</div>
		<div class="span3">
			<div class="border_1"></div>
		</div>
		<div class="span3">
			<div class="border_2"></div>
		</div>
		<div class="span3">
			<div class="border_4"></div>
		</div>
	</div>
	<?php
			}
		}
	?>
</div>

<?php get_footer(); ?>