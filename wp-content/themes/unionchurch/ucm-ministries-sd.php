<?php
/*
Template Name: Ministry - Small Discipleship
*/
?>

<?php get_header(); ?>

<div id="post-<?php the_ID(); ?>" class="container ucp_page ministries sd">
	<?php 
		if( have_posts() )
		{
			while( have_posts() )
			{
				the_post();
	?>
	<div class="row-fluid">
		<div class="span3">
			<?php get_template_part('ucm', 'left-nav'); ?>
		</div>
		
		<div class="span9">
			<div class="banner">
			<div class="ucp_main_title1"><?php echo the_title() ?></div>
			<?php 
				if ( has_post_thumbnail() )
				{
					the_post_thumbnail();
				}
				else // show default banner
				{
					echo '<img src="'. get_template_directory_uri().'/images/ucm_ministries.jpg" />';
				}
			?>
			</div>
			<div class="spacing"></div>
			<div class="content_container">
			<div class="content">
				<?php 
					the_content();
					
					$args = array(
								'orderby' => 'menu_order',
								'order' => 'ASC',
								'post_type' => 'ucm_discipleship',
								'post_status' => 'publish',
								);
					
					$sd_groups = new WP_Query( $args );
				?>
				<div class="group">
					<div class="slider">
						<img class="slide_nav left_" src="<?php echo get_template_directory_uri().'/images/arrow_left.png' ?>">
						<img class="slide_nav right_"src="<?php echo get_template_directory_uri().'/images/arrow_right.png' ?>">
						
						<div class="items_cont">
							<?php 
								foreach($sd_groups->posts as $index => $post)
								{
									setup_postdata($post);
									$show = ($index < 2) ? 'show' : '';
							?>
								<div class="item_  <?php echo $show ?>">
									<div class="bg">
										<div class="name"><?php echo the_title() ?></div>
										<?php
										echo get_field('ucmd_group_type') ? '<div class="type">'.get_field('ucmd_group_type').'</div>' : '';
										echo get_field('ucmd_meeting_time') ? '<div class="time">'.get_field('ucmd_meeting_time').'</div>' : '';
										echo get_field('ucmd_general_group_locaton') ? '<div class="group_locaton">'.get_field('ucmd_general_group_locaton').'</div>' : '';
										echo get_field('ucmd_group_status') ? '<div class="status">'.get_field('ucmd_group_status').'</div>' : '';
										?>
										<div class="show_map" data-index="<?php echo $index ?>" >SHOW MAP</div>
									</div>
								</div>
							<?php 
								}
							?>
						</div>
					</div>
					<div class="clear_both"></div>
					<div class="show_all_markers">SHOW ALL</div>
					<div class="ucm_sd_map">
						<?php 
							$markers = array();
							foreach($sd_groups->posts as $index => $post)
							{
								setup_postdata($post);
								$location = get_field('ucmd_map_coordinates');
								if( !empty($location) )
								{
									$markers[] = get_field('ucmd_map_coordinates');
									/*<div class="marker" data-lat="<?php echo $location['lat']; ?>" data-lng="<?php echo $location['lng']; ?>"></div>*/
						?>
							
						<?php
								}
							}
						?>
					</div>
				</div>
			</div>
			</div>
		</div>
	</div>
	<div class="row-fluid border_group">
		<div class="span3">
			<div class="border_4"></div>
		</div>
		<div class="span3">
			<div class="border_1"></div>
		</div>
		<div class="span3">
			<div class="border_2"></div>
		</div>
		<div class="span3">
			<div class="border_3"></div>
		</div>
	</div>
	<?php
			}
		}
	?>
</div>
<script type="text/javascript" src="<?php echo get_template_directory_uri().'/js/nav_resize.js'; ?>"></script>
<script src="https://maps.googleapis.com/maps/api/js?v=3.exp&sensor=false"></script>
<script type="text/javascript" src="<?php echo get_template_directory_uri().'/js/discipleship_google_map.js'; ?>"></script>
<script>
sd_group = <?php echo json_encode($markers) ?>;
</script>
<?php get_footer(); ?>