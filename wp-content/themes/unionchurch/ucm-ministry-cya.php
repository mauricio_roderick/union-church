<?php
/*
Template Name: Ministry - Age & Stage - College & Young Adults
*/
?>

<?php get_header(); ?>

<div id="post-<?php the_ID(); ?>" class="container ucp_page ministries">
	<?php 
		if( have_posts() )
		{
			while( have_posts() )
			{
				the_post()
	?>
	<div class="row-fluid">
		<div class="span3">
			<?php get_template_part('ucm', 'left-nav'); ?>
		</div>
		
		<div class="span9">
			<div class="banner">
			<div class="ucp_main_title1"><?php echo the_title() ?></div>
			<?php 
				if ( has_post_thumbnail() )
				{
					the_post_thumbnail();
				}
				else // show default banner
				{
					echo '<img src="'. get_template_directory_uri().'/images/ministries_banner.jpg" />';
				}
			?>
			</div>
			<div class="spacing"></div>
			<div class="content_container">
				<div class="content listing">
					<?php
						the_content();
						$page = (get_query_var('page')) ? get_query_var('page') : 1;
						
						$args = array(
								'orderby' => 'menu_order',
								'order' => 'ASC',
								'post_type' => 'ucm_ministry',
								'category__in' => array(12),
								'post_status' => 'publish',
								'posts_per_page' => get_option( 'posts_per_page', 5 ),
								'paged' => $page,
								);
								
						$pastors_posts = new WP_Query( $args );
						
						$paginate_links = paginate_links( 
												array(
												'base' => site_url('ministries/age-stage/college-young-adults/%_%'),
												'format' => '?page=%#%',
												'total' => $pastors_posts->max_num_pages,
												'current' => $page,
												'type' => 'list',
												)
											);
						
						foreach ( $pastors_posts->posts as $post ) 
						{
							setup_postdata($post);
							get_template_part('ucm', 'age-stage-item');
						} 
						
						wp_reset_postdata();
						echo ucm_pagination($paginate_links);
						?>
				</div>
			</div>
		</div>
	</div>
	<div class="row-fluid border_group">
		<div class="span3">
			<div class="border_4"></div>
		</div>
		<div class="span3">
			<div class="border_1"></div>
		</div>
		<div class="span3">
			<div class="border_2"></div>
		</div>
		<div class="span3">
			<div class="border_3"></div>
		</div>
	</div>
	<?php
			}
		}
	?>
</div>
<script type="text/javascript" src="<?php echo get_template_directory_uri().'/js/nav_resize.js'; ?>"></script>

<?php get_footer(); ?>