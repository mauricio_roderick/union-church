<div class="item">
	<div class="pic">
		<?php 
			$image = get_field('staff_image');
			$email = get_field('staff_email');
			$image_src_relative = str_replace(site_url().'/', '',$image['sizes']['medium']);
			$image_src = ($image && file_exists($image_src_relative)) ? $image['sizes']['medium'] : get_template_directory_uri().'/images/img_sub.png'
		?>
		<img class="img-circle" src="<?php echo $image_src ?>" />
	</div>
	<div class="dtls">
		<div class="name"><?php echo the_title() ?></div>
		<div class="title"><?php echo the_field('staff_title') ?></div>
		<div class="links">
			<?php if($email){ ?>
				<i class="fa fa-envelope"></i><a href="<?php echo "mailto:$email" ?>">EMAIL</a>
			<?php } ?>
			<?php if($post->post_content){ ?>
				<i class="fa fa-user"></i><a href="<?php echo get_permalink() ?>">BIO</a>
			<?php } ?>
		</div>
	</div>
	<div class="clear_both"></div>
</div>