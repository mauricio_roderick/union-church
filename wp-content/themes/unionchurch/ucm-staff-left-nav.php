<div class="nav_top"></div>
<div class="clear_both spacing"></div>
<div class="nav_">
		<?php 
			$page_children = array(
					'sort_order' => 'ASC',
					'sort_column' => 'menu_order',
					'child_of' => 28,
					'parent' => 28,
					'post_status' => 'publish',
				); 
				
			$page_children = get_pages($page_children); 
			foreach($page_children as $index => $page)
			{
		?>
		
		<div class="item">
			<?php 
				$sub_page_children = array(
						'sort_order' => 'ASC',
						'sort_column' => 'menu_order',
						'child_of' => $page->ID,
						'parent' => $page->ID,
						'post_status' => 'publish',
					); 
					
				$sub_page_children = get_pages($sub_page_children);
				
				$parent_class = (count($sub_page_children)) ? 'class="parent"' : '';
			?>
			<a href="<?php echo site_url($page->post_name) ?>" <?php echo $parent_class ?> ><?php echo $page->post_title ?></a>
			<?php if(count($sub_page_children) > 0) { ?>
			<div class="sub_nav">
				<?php foreach($sub_page_children as $index => $sub_page){ ?>
					<a href="<?php echo site_url($sub_page->post_name) ?>"><?php echo $sub_page->post_title ?></a>
				<?php } ?>
			</div>
			<?php } ?>
		</div>
		
		<?php 
			}
		?>
</div>