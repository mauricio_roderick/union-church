<?php
/*
Template Name: Staff - Support
*/
?>

<?php get_header(); ?>

<div id="post-<?php the_ID(); ?>" class="container ucp_page gtku staff">
	<?php 
		if( have_posts() )
		{
			while( have_posts() )
			{
				the_post()
	?>
	<div class="row-fluid">
		<div class="span3">
			<?php get_template_part('ucm', 'staff-left-nav'); ?>
		</div>
		
		<div class="span9">
			<div class="banner">
			<div class="ucp_main_title1"><?php echo the_title() ?></div>
			<?php 
				if ( has_post_thumbnail() )
				{
					the_post_thumbnail();
				}
				else // show default banner
				{
					echo '<img src="'. get_template_directory_uri().'/images/ucm_our_staff.jpg" />';
				}
			?>
			</div>
			<div class="clear_both spacing"></div>
			<div class="content_container">
				<div class="content listing">
					<?php
						$page = (get_query_var('page')) ? get_query_var('page') : 1;
						
						$args = array(
								'orderby' => 'menu_order',
								'order' => 'ASC',
								'post_type' => 'ucm_staff',
								'category__in' => array(4),
								'post_status' => 'publish',
								'posts_per_page' => 20,
								'paged' => $page,
								);
								
						$pastors_posts = new WP_Query( $args );
						
						$paginate_links = paginate_links( 
												array(
												'base' => site_url('get-to-know-us/support-staff/%_%'),
												'format' => '?page=%#%',
												'total' => $pastors_posts->max_num_pages,
												'current' => $page,
												'type' => 'list',
												)
											);
						
						foreach ( $pastors_posts->posts as $post ) 
						{
							setup_postdata($post);
							$trimmed_content = wp_trim_words( $post->post_content, 75, '...<a href="'. get_permalink() .'"> continue reading</a>' );
							get_template_part('ucm', 'staff-item');
						} 
						
						wp_reset_postdata();
						echo ucm_pagination($paginate_links);
					?>
				</div>
			</div>
		</div>
	</div>
	<div class="row-fluid border_group">
		<div class="span3">
			<div class="border_1"></div>
		</div>
		<div class="span3">
			<div class="border_2"></div>
		</div>
		<div class="span3">
			<div class="border_3"></div>
		</div>
		<div class="span3">
			<div class="border_4"></div>
		</div>
	</div>
	<?php
			}
		}
	?>
</div>
<script type="text/javascript" src="<?php echo get_template_directory_uri().'/js/greyscale.js'; ?>"></script>
<script type="text/javascript" src="<?php echo get_template_directory_uri().'/js/nav_resize.js'; ?>"></script>

<?php get_footer(); ?>